select unit_id, [name] from unit

select [unit].[name], count(personal_cards.registration_id) as [count] from unit
join personal_cards on personal_cards.unit_id = unit.unit_id
join documents on documents.registration_id = personal_cards.registration_id
join document_types on document_types.[type_id] = documents.[type_id]
where document_types.name = '�i���������'
group by unit.name

select count(personal_cards.registration_id) as [count] from personal_cards
join documents on documents.registration_id = personal_cards.registration_id
join document_types on document_types.[type_id] = documents.[type_id]
where document_types.name = '�i�������� ����' and 
end_date> dateadd(month, datediff(month, 0, getdate())-1, 0)

select distinct personal_cards.[name], surname from personal_cards
join documents on documents.registration_id = personal_cards.registration_id
join document_types on document_types.[type_id] = documents.[type_id]
where document_types.name = '�i�������� ����'
order by name asc, surname asc

insert into document_types (name) values ('�����')
update document_types set [name]='�������' where [type_id]=3

insert into documents (start_date, end_date, type_id, registration_id) values
(getdate(), getdate(), 3, 1)
update documents set type_id = 2 where document_id = 5

insert into personal_cards (surname, name, middle_name, birth_year, id_code, unit_id, position_id,enrollment_date) values
('Ivanov', 'Ivan', 'Ivanovich', '2002', 5959, 2, 1, getdate())
update personal_cards set [name] = 'Yes' where registration_id = 5

insert into positions (name, salary) values ('Manager', 100)
update positions set [name]='Scrum' where positions.position_id=9

insert into unit (name) values ('Omega')
update unit set [name]='Delta2' where [unit_id]=5

update documents
set [type_id] = 1
where [type_id] = 3

insert into documents (start_date, end_date, type_id, registration_id)
select getdate(), getdate(), 3, personal_cards.registration_id from personal_cards
where unit_id>1

declare @salary int =500, @brith int =2002, @doc_type nvarchar(50) = '�i���������',
@team nvarchar(50) = 'alpha', @pos nvarchar(50) = 'junior'

Select surname, [dbo].[personal_cards].[name], middle_name, birth_year, id_code, [dbo].[positions].[name], salary from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
where salary > @salary and birth_year <= @brith

select document_id, start_date, end_date, registration_id, [name] from dbo.documents
inner join dbo.document_types on dbo.document_types.type_id =dbo.documents.type_id
where dbo.document_types.name = @doc_type and end_date < GETDATE()

select registration_id, surname, [dbo].[personal_cards].[name], birth_year, id_code,dbo.positions.name, dbo.unit.name salary, enrollment_date from dbo.personal_cards
inner join dbo.unit on dbo.unit.unit_id = dbo.personal_cards.unit_id
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
where [dbo].[positions].[name] = @pos and [dbo].[unit].[name] = @team

select * from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
where middle_name like '%ovich' and dbo.positions.name = @pos

Select sum(salary) from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
where salary > @salary and dbo.positions.name = @pos

Select surname, salary from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
inner join dbo.unit on dbo.unit.unit_id = dbo.personal_cards.unit_id
where salary < @salary
order by salary

Select surname, unit.name, salary from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
inner join dbo.unit on dbo.unit.unit_id = dbo.personal_cards.unit_id
where unit.name = @team and salary > @salary
order by salary