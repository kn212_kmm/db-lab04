select sum(NaSklade) as tovar_count from tovar

select count(id_sotrud) as sotrudnik_count from sotrudnik

select count(id_postach) as postachalnik_count from postachalnik

select sum(zakaz_tovar.Kilkist) as [count], zakaz_tovar.id_tovar
from zakaz_tovar
join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
where date_naznach >= dateadd(month, datediff(month, 0, getdate())-1, 0)
group by zakaz_tovar.id_tovar

select sum(Price * Kilkist) as cost
FROM zakaz_tovar
join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
where date_naznach >= dateadd(month, datediff(month, 0, getdate())-1, 0)

select sum(Price * Kilkist) as cost, postachalnik.id_postach
from postachalnik
join tovar on tovar.id_postav = postachalnik.id_postach
join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
group by postachalnik.id_postach

select count(zakaz.id_zakaz) as [count], M.id_postach
from zakaz
join zakaz_tovar on zakaz_tovar.id_tovar = zakaz.id_zakaz
join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
join (
	select postachalnik.id_postach from postachalnik
	join tovar on tovar.id_postav = postachalnik.id_postach
	where tovar.Nazva = '������'
	group by postachalnik.id_postach
) 
M on M.id_postach = tovar.id_postav
group by M.id_postach

select avg(Price) as [avg], Nazva
from tovar
join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
group by Nazva

select sum(Price * Kilkist) as cost
from klient
join zakaz on zakaz.id_klient = klient.id_klient
join zakaz_tovar on zakaz_tovar.id_zakaz = zakaz.id_zakaz
join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
where City = '�������'

select avg(Price) as [avg], id_postach
from postachalnik
join tovar on id_postav = id_postach
group by id_postach