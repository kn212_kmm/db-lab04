
select Name_ini, Nazva, sum(Reiting) AS Summarniy_raiting
from dbo_student
join Reiting on Kod_student = Kod_stud
join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
group by Nazva, dbo_student.Name_ini

select dbo_groups.Kod_group, count(dbo_student.Kod_group) as [�ount]
from dbo_groups
join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
group by dbo_groups.Kod_group

select Tmp.Kod_group, count(predmet.K_predmet) as [count]
from predmet join (
select distinct dbo_groups.Kod_group, K_predmet
	from dbo_groups
	join Rozklad_pids on Rozklad_pids.Kod_group = dbo_groups.Kod_group
	join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
) Tmp on Tmp.K_predmet = predmet.K_predmet
group by Tmp.Kod_group

select dbo_groups.Kod_group, count(predmet.K_predmet) as [count]
from dbo_groups 
join Rozklad_pids on Rozklad_pids.Kod_group = dbo_groups.Kod_group
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
group by dbo_groups.Kod_group

select dbo_groups.Kod_group, avg(Reiting) as [avg]
from dbo_groups
join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
join Reiting on Kod_student = Kod_stud
group by dbo_groups.Kod_group

select Nazva, avg(Reiting) as [avg]
from predmet
join Predmet_plan on Predmet_plan.K_predmet = predmet.K_predmet
join Rozklad_pids on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
join Reiting on Reiting.K_zapis = Rozklad_pids.K_zapis
group by Nazva

select Name_ini, Nazva, Reiting
from dbo_student
join Reiting on Kod_student = Kod_stud
join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on predmet.K_predmet = Predmet_plan.K_predmet

select distinct Nazva, min(Reiting) over (partition BY Nazva) as [min]
from dbo_student 
join Reiting on Kod_student = Kod_stud
join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on predmet.K_predmet = Predmet_plan.K_predmet

select distinct Nazva, max(Reiting) over (partition BY Nazva) as [max]
from dbo_student 
join Reiting on Kod_student = Kod_stud
join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on predmet.K_predmet = Predmet_plan.K_predmet

select Nazva,
sum(Chas_Lek) as Lektion,
sum(Cahs_pr) as Practic,
sum(Cahs_sam) as Saminar,
sum(Chas_Labor) as Lab,
sum(Chas_all) as [All]
from predmet 
join Predmet_plan on Predmet_plan.K_predmet = predmet.K_predmet
group by Nazva

select Nazva, count(Kod_group) as [count]
from dbo_groups 
join Navch_plan on Navch_plan.K_navch_plan = dbo_groups.K_navch_plan
join Spetsialnost on Spetsialnost.K_spets = Navch_plan.K_spets
group by Nazva;

declare @surname nvarchar(50) ='name', @id int =5, @group nvarchar(50), @module int = 15, @predmet int = 1

delete Reiting from Reiting 
where Kod_student = @id  

delete Rozklad_pids from Rozklad_pids
join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
where Predmet_plan.K_predmet = @id

update Reiting set Reiting*=1.15;

update Reiting set Reiting/=1.15;

insert into Reiting (K_zapis, Kod_student, Reiting, Prisutn)
select 5, Kod_stud, 70, 0 from dbo_student
where Kod_group = @group

--���� ������i ����

update Predmet_plan
set Kilk_modul = @module
from Predmet_plan
where K_predmet = @predmet

delete dbo_student
from dbo_student
where Kod_group=@group

insert into Reiting (K_zapis, Kod_student, Reiting, Prisutn)
select 1, Kod_stud, 60, 0
from dbo_student
where Kod_group = @group

update Reiting
set Prisutn='true'
from dbo_student 
join Reiting on Kod_student = Kod_stud
where Sname= @surname